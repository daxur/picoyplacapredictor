// // Pico y Placa Logic
window.onload = function(){
  function checkFunction(){
    //Get the values of plate number, date and time as a astring.
    var strNumber = document.getElementById("num").value;
    var strDate = document.getElementById("date").value;
    var strTime = document.getElementById("time").value;

    //Pass the string value of time to an integer value (where 14:00 is 1400).
    var strTime1 = strTime.slice(0,2);
    var strTime2 = strTime.slice(3,5);
    var intTime = parseInt(strTime1 + strTime2, 10);

    //Get the day from the selected date.
    var date = new Date(strDate);
    var day = date.getDay();

    //Get the last digit of the plate.
    var arrayNumber  = strNumber.split("");
    var lastDigit = arrayNumber.pop();     
  
    var foto = document.getElementById("img");
    //Checks the time when "Pico y Placa" applies, according of the last digit of the number plate and the 
    //day of the week.
    if ((intTime >= 700 && intTime <= 930) || (intTime >= 1600 && intTime <= 1930)) {
      var dia;
      switch (parseInt(lastDigit, 10)) {
        case 1: case 2:
          d = "Monday";
          dia = 0;
          break;
        case 3: case 4:
          d = "Tuesday";
          dia = 1;
          break;
        case 5: case 6:
          d = "Wednesday";
          dia = 2;
          break;
        case 7: case 8:
          d = "Thursday";
          dia = 3;
          break;
        case 9: case 0:
          d = "Friday";
          dia = 4;
          break;
      }
      if (dia == day) {
        document.getElementById("result").innerHTML='Your car CANNOT mobilize on ' + d + ' at the selected time';
        img.removeAttribute("hidden");
        foto.src="no_move.png";
      }
      else{
        document.getElementById("result").innerHTML='Your car CAN mobilize normally.';
        img.removeAttribute("hidden");
        foto.src="move.png";
      }
    }
    else {
      document.getElementById("result").innerHTML='Your car CAN mobilize normally.';
      img.removeAttribute("hidden");
      foto.src="move.png";
    }
  }
  //Calling function when "Check" button is pressed.
  document.getElementById("check").onclick = function(){
    //Validation of number plate.
    var strNumber = document.getElementById("num").value;
    var exp = new RegExp("^[0-9]{3,4}$");
    if (exp.test(strNumber)) {
      checkFunction();
    } else {
      document.getElementById("result").innerHTML='The number plate must have 3 or 4 digits';
      var foto = document.getElementById("img");
      img.setAttribute('hidden','hidden');
    }
  }
};